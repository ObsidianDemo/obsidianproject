
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr"
      xmlns:content="http://purl.org/rss/1.0/modules/content/"
      xmlns:dc="http://purl.org/dc/terms/"
      xmlns:foaf="http://xmlns.com/foaf/0.1/"
      xmlns:og="http://ogp.me/ns#"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      xmlns:sioc="http://rdfs.org/sioc/ns#"
      xmlns:sioct="http://rdfs.org/sioc/types#"
      xmlns:skos="http://www.w3.org/2004/02/skos/core#"
      xmlns:xsd="http://www.w3.org/2001/XMLSchema#">

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="https://obsidian.co.za/sites/default/files/obs-logo-ico.png" type="image/png" />
  <link rel="shortlink" href="/node/5" />
  <link rel="canonical" href="/content/leading-open-technology-solutions" />
  <meta name="Generator" content="Drupal 7 (http://drupal.org)" />
  <title>Leads Atlassian technology requirements | Obsidian Systems</title>
  <style type="text/css" media="all">
    @import url("https://obsidian.co.za/modules/system/system.base.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/system/system.menus.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/system/system.messages.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/system/system.theme.css?q2rkp4");
  </style>
  <style type="text/css" media="all">
    @import url("https://obsidian.co.za/modules/field/theme/field.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/node/node.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/search/search.css?q2rkp4");
    @import url("https://obsidian.co.za/modules/user/user.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/modules/views/css/views.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/modules/ckeditor/css/ckeditor.css?q2rkp4");
  </style>
  <style type="text/css" media="all">
    @import url("https://obsidian.co.za/sites/all/libraries/animate/animate.min.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/modules/colorbox/styles/default/colorbox_style.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/modules/ctools/css/ctools.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/libraries/sidr/stylesheets/jquery.sidr.dark.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/libraries/fontawesome/css/font-awesome.css?q2rkp4");
  </style>
  <style type="text/css" media="all">
    @import url("https://obsidian.co.za/sites/all/themes/obsidian/css/structure.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/themes/obsidian/css/style.css?q2rkp4");
    @import url("https://obsidian.co.za/sites/all/themes/obsidian/css/layout.css?q2rkp4");
  </style>
  <style type="text/css" media="all">
    <!--/*--><![CDATA[/*><!--*/
    .slide01{background-color:#232628 !important;background-image:url('https://obsidian.co.za/sites/default/files/bg.png') !important;background-repeat:no-repeat !important;background-attachment:scroll !important;background-position:center center !important;background-size:cover !important;-webkit-background-size:cover !important;-moz-background-size:cover !important;-o-background-size:cover !important;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.https://obsidian.co.za/sites/default/files/bg.png',sizingMethod="scale");-ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://obsidian.co.za/sites/default/files/bg.png',sizingMethod='scale')";}

    /*]]>*/-->
  </style>
  <script type="text/javascript" src="https://obsidian.co.za/misc/jquery.js?v=1.4.4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/misc/jquery-extend-3.4.0.js?v=1.4.4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/misc/jquery.once.js?v=1.2"></script>
  <script type="text/javascript" src="https://obsidian.co.za/misc/drupal.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/libraries/colorbox/jquery.colorbox-min.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/modules/colorbox/js/colorbox.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/modules/colorbox/styles/default/colorbox_style.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/modules/responsive_menus/styles/sidr/js/responsive_menus_sidr.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/libraries/sidr/jquery.sidr.min.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/modules/google_analytics/googleanalytics.js?q2rkp4"></script>
  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-56388940-1", {"cookieDomain":"auto"});ga("set", "anonymizeIp", true);ga("send", "pageview");
    //--><!]]>
  </script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/themes/obsidian/js/classie.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/themes/obsidian/js/viewportchecker.js?q2rkp4"></script>
  <script type="text/javascript" src="https://obsidian.co.za/sites/all/themes/obsidian/js/viewporter.js?q2rkp4"></script>
  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"thetheme","theme_token":"VIjTBHHxRWeryVlyXozV3nIxO7h1_bseuW_gJUwmyqA","js":{"misc\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/responsive_menus\/styles\/sidr\/js\/responsive_menus_sidr.js":1,"sites\/all\/libraries\/sidr\/jquery.sidr.min.js":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"0":1,"sites\/all\/themes\/obsidian\/js\/classie.js":1,"sites\/all\/themes\/obsidian\/js\/viewportchecker.js":1,"sites\/all\/themes\/obsidian\/js\/viewporter.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/libraries\/animate\/animate.min.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/libraries\/sidr\/stylesheets\/jquery.sidr.dark.css":1,"sites\/all\/libraries\/fontawesome\/css\/font-awesome.css":1,"sites\/all\/themes\/obsidian\/css\/structure.css":1,"sites\/all\/themes\/obsidian\/css\/style.css":1,"sites\/all\/themes\/obsidian\/css\/layout.css":1,"0":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"better_exposed_filters":{"views":{"home_page_header_content":{"displays":{"block":{"filters":[]},"block_1":{"filters":[]}}},"slide_01_background":{"displays":{"block":{"filters":[]}}},"home_page_clients":{"displays":{"block":{"filters":[]}}},"home_page_line_items":{"displays":{"block":{"filters":[]}}},"home_page_main_sections":{"displays":{"block":{"filters":[]}}},"testimonials":{"displays":{"block":{"filters":[]}}},"footer_links":{"displays":{"block":{"filters":[]},"block_2":{"filters":[]}}},"footer_contact_details":{"displays":{"block":{"filters":[]}}},"footer_copyright_and_social":{"displays":{"block":{"filters":[]}}}}},"responsive_menus":[{"selectors":["#sidr-container"],"trigger_txt":"\u003Cspan\u003E\u003C\/span\u003E","side":"right","speed":"200","media_size":"400000","displace":"1","renaming":"1","onOpen":"","onClose":"","responsive_menus_style":"sidr"}],"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1}});
    //--><!]]>
  </script>
  <script>
    function init() {
      window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 175,
                header = document.querySelector("header");
        if (distanceY > shrinkOn) {
          classie.add(header,"darker");
        } else {
          if (classie.has(header,"darker")) {
            classie.remove(header,"darker");
          }
        }
      });
    }
    window.onload = init();
  </script>
</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-5 node-type-home-page" >
<div id="skip-link">
  <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
</div>
<div title="top" id="home"></div>

<div id="area-menu-wrap">
  <header id="the-menu" class="area-menu">
    <div class="container">
      <div class="the-logo">
        <a href="/" title="Home" rel="home" id="logo">
          <img src="https://obsidian.co.za/sites/default/files/logo.png" alt="Home" />
        </a>
      </div>
      <div class="menu-nav">
        <div class="region region-logo-nav">
          <div id="block-system-main-menu" class="block block-system block-menu">


            <div class="content">
              <ul class="menu"><li  class="first leaf home"><a href="/" class="active">Home</a></li>
                <li  class="leaf about"><a href="/about">About</a></li>
                <li  class="leaf products"><a href="/products">Products</a></li>
                <li  class="leaf blog"><a href="/blog">Blog</a></li>
                <li  class="last leaf contact"><a href="/contact">Contact</a></li>
              </ul>  </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearer"></div>
    <div id="sidr-container">  <div class="region region-sidr">
      <div id="block-menu-menu-sidr-menu" class="block block-menu">


        <div class="content">
          <ul class="menu"><li  class="first leaf home"><a href="/" class="active">Home</a></li>
            <li  class="leaf about"><a href="/about">About</a></li>
            <li  class="expanded products"><a href="/products">Products</a><ul class="menu"><li  class="first leaf smarterteams smarterteams"><a href="/products/smarterteams">SmarterTeams</a></li>
              <li  class="leaf smarterdata smarterdata"><a href="/products/smarterdata">SmarterData</a></li>
              <li  class="leaf smartercode smartercode"><a href="/products/smartercode">SmarterCode</a></li>
              <li  class="last leaf smartercompute smartercompute"><a href="/products/smartercompute">SmarterCompute</a></li>
            </ul></li>
            <li  class="leaf blog"><a href="/blog">Blog</a></li>
            <li  class="last leaf contact"><a href="/contact">Contact</a></li>
          </ul>  </div>
      </div>
    </div>
    </div>	<div class="clearer"></div>
  </header>
</div>

<div id="slide01-wrap" class="slide01">
  <div class="slide01-container">
    <h1 class="title" id="page-title">Leading open technology solutions</h1>	      <div class="region region-slide01">
    <div id="block-views-home-page-header-content-block" class="block block-views">


      <div class="content">
        <div class="view view-home-page-header-content view-id-home_page_header_content view-display-id-block view-dom-id-19729644a6031f226998d07df20fa695">



          <div class="view-content">
            <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">

              <div class="views-field views-field-field-subtitle">        <div class="field-content"><div class="subtitle">We help teams to get their code to the best compute and the correct data type</div></div>  </div>
              <div class="views-field views-field-field-header-button">        <div class="field-content"><div class="header-button"><a class="button" href="contact">Contact Us</a></div></div>  </div>  </div>
          </div>






        </div>  </div>
    </div>
    <div id="block-views-slide-01-background-block" class="block block-views">


      <div class="content">
        <div class="view view-slide-01-background view-id-slide_01_background view-display-id-block view-dom-id-3a6264d7a47adb3f64b44609f1f9d26a">



          <div class="view-content">
            <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">

              <div class="views-field views-field-field-background-image">        <div class="field-content"></div>  </div>  </div>
          </div>






        </div>  </div>
    </div>
    <div id="block-views-home-page-clients-block" class="block block-views">


      <div class="content">
        <div class="view view-home-page-clients view-id-home_page_clients view-display-id-block clients view-dom-id-fa1122da68ae17baf31475bac181f260">
          <div class="view-header">
            <div class="clients-text">Providing some of the biggest software solutions in the world</div>
          </div>



          <div class="view-content">
            <div class="views-row views-row-1 views-row-odd views-row-first client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://agilecraft.com/"><div class="client-item" title="AgileCraft"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/01%20agilecraft.png" width="240" height="167" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-2 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.atlassian.com/"><div class="client-item" title="Atlassian"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/02%20atlassian.png" width="150" height="167" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-3 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.chef.io"><div class="client-item" title="Chef"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/03%20chef.png" width="434" height="163" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-4 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.dataguise.com/"><div class="client-item" title="Dataguise"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/05%20dataguise.png" width="354" height="73" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-5 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.docker.com"><div class="client-item" title="Docker"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/06%20docker.png" width="199" height="167" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-6 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.enterprisedb.com"><div class="client-item" title="EnterpriseDB"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/07%20edb.png" width="266" height="153" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-7 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.hashicorp.com/"><div class="client-item" title="HashiCorp"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/08%20hashicorp.png" width="524" height="113" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-8 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.cloudera.com/"><div class="client-item" title="Cloudera"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/cloudera.png" width="344" height="66" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-9 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.icinga.com/"><div class="client-item" title="Icinga"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/10%20icinga.png" width="364" height="127" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-10 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.mongodb.com/"><div class="client-item" title="MongoDB"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/11%20mongodb.png" width="468" height="124" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-11 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.redhat.com"><div class="client-item" title="Red Hat"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/12%20redhat.png" width="431" height="123" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-12 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.stratus.com/"><div class="client-item" title="Stratus"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/13%20stratus.png" width="254" height="159" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-13 views-row-odd client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.suse.com/"><div class="client-item" title="Suse"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/image_preview.png" width="154" height="83" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-14 views-row-even client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.talend.com/"><div class="client-item" title="Talend"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/15%20talend.png" width="341" height="82" alt="" /></div></a></div>  </div>  </div>
            <div class="views-row views-row-15 views-row-odd views-row-last client-item-wrap">

              <div class="views-field views-field-field-client-logo">        <div class="field-content"><a href="https://www.zimbra.com/"><div class="client-item" title="Zimbra"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/16%20zimbra.png" width="406" height="87" alt="" /></div></a></div>  </div>  </div>
          </div>






        </div>  </div>
    </div>
  </div>
  </div>
</div>



<div class="container"><div class="tabs"></div></div>






<div id="wrap" class="main-area">
  <div class="container">

    <div id="content" class="sixteen columns clearfix">


      <div id="main">


        <div class="region region-content">
          <div id="block-system-main" class="block block-system">


            <div class="content">
              <div  about="/content/leading-open-technology-solutions" typeof="sioc:Item foaf:Document" class="ds-1col node node-home-page node-promoted view-mode-full clearfix">


                <div class="home-body"><h2>Collaborate, Architect, Implement &amp; Manage</h2>
                  <p>Obsidian designs technology solutions using the best fit software and vendor partners to help you achieve your individual goals, team deliverables and business objectives from large enterprises to bespoke companies.</p>
                </div></div>

            </div>
          </div>
          <div id="block-views-home-page-line-items-block" class="block block-views">


            <div class="content">
              <div class="view view-home-page-line-items view-id-home_page_line_items view-display-id-block home-page-line-items view-dom-id-db8893278a15d0c42dd1c870e0c866b8">



                <div class="view-content">
                  <div class="views-responsive-grid views-responsive-grid-horizontal views-columns-2">
                    <div class="views-row clearer views-row-1 views-row-first">
                      <div class="one_half views-column-1">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/collaborate.png" width="106" height="102" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Collaborate</h3></div>
                            <div class="hpli-description">A collaborative approach with our customers will result in the discovery of solutions for adoption by you and your team that will address critical business opportunities and solve problems.</div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                      <div class="one_half views-column-2 last">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/architect.png" width="106" height="102" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Architect</h3></div>
                            <div class="hpli-description">Our partner base allowsss you to select the best tech arsenal to meet your team, data, and compute needs with the expertise to deliver an integrated solution that is both future-facing and flexible. </div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                    </div>
                    <div class="views-row clearer views-row-2">
                      <div class="one_half views-column-1">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/Implement.png" width="106" height="102" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Implement</h3></div>
                            <div class="hpli-description">working with your team to systematically implement an integrated solution based on vendor best practice and your organisation&#039;s standards that will add value to your end users and customers.</div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                      <div class="one_half views-column-2 last">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/ManagedServices.png" width="106" height="102" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Managed Services</h3></div>
                            <div class="hpli-description">We have proven expertise to maintain your critical infrastructure and address software needs. This ensures you benefit from a managed view of your systems and frees up valuable time to focus on forward-thinking projects.</div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                    </div>
                    <div class="views-row clearer views-row-3 views-row-last">
                      <div class="one_half views-column-1">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/home-ico-03.png" width="106" height="72" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Software Procurement</h3></div>
                            <div class="hpli-description">We have a dedicated and specialised team in place to remove many of the administrative burdens when engaging with vendors. We can also assist in obtaining the best deals and value-added services for your software licenses.</div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                      <div class="one_half views-column-2 last">

                        <div class="views-field views-field-field-hpli-title">        <div class="field-content"><div class="home-page-line-item">
                          <div class="hpli-icon"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/Training.png" width="106" height="102" alt="" /></div>
                          <div class="hpli-content">
                            <div class="hpli-title"><h3>Training</h3></div>
                            <div class="hpli-description">We want your teams to get the best out of technology solutions and to do this we have certified trainers and training courses to help you to build your own skills and develop internal resources.</div>
                          </div>
                          <div class="clearer"></div>
                        </div></div>  </div>    </div>
                    </div>
                  </div>
                </div>






              </div>  </div>
          </div>
        </div>
      </div>
    </div><!-- /#content -->


    <div class="clear"></div>

  </div><!-- /.container -->
</div><!-- /#wrap -->

<div id="slide04-wrap" class="slide04">
  <div class="container">
    <div class="region region-slide04">
      <div id="block-views-home-page-main-sections-block" class="block block-views">


        <div class="content">
          <div class="view view-home-page-main-sections view-id-home_page_main_sections view-display-id-block home-page-main-sections view-dom-id-382bdf411393f50e1f679deed953d515">



            <div class="view-content">
              <div class="views-responsive-grid views-responsive-grid-horizontal views-columns-2">
                <div class="views-row clearer views-row-1 views-row-first">
                  <div class="one_half views-column-1">

                    <div class="views-field views-field-title">        <span class="field-content"><div class="home-page-main-section-item">
<div class="hpmsi-image"><a href="/products"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/home-products.png" width="1127" height="636" alt="" /></a></div>
<div class="hpli-icon"><a href="/products"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/home-products-ico.png" width="61" height="75" alt="" /></a></div>
<div class="hpli-content">
<div class="hpli-title"><h3><a href="https://obsidian.co.za/products">Products</a></h3></div>
<div class="hpli-description">We have a global network of vendors providing the best enterprise-ready software solutions that are interoperable, secure, flexible, and open.</div>
<div class="hpmsi-link"><a href="https://obsidian.co.za/products" class="button">Read More <i class="fa fa-arrow-right"></i></a></div>
</div>
<div class="clearer"></div>
</div></span>  </div>    </div>
                </div>
              </div>
            </div>






          </div>  </div>
      </div>
    </div>
  </div>
</div>

<div id="slide05-wrap" class="slide05">
  <div class="container">
    <div class="region region-slide05">
      <div id="block-views-testimonials-block" class="block block-views">


        <div class="content">
          <div class="view view-testimonials view-id-testimonials view-display-id-block testimonials view-dom-id-28077c6f2787df50547561adc854a17c">



            <div class="view-content">
              <div class="views-responsive-grid views-responsive-grid-horizontal views-columns-3">
                <div class="views-row clearer views-row-1 views-row-first">
                  <div class="one_third views-column-1">

                    <div class="views-field views-field-title">        <span class="field-content"><div class="testimonial-item">
<div class="testimonial-body"><p>We are growing our knowledge base to assist you and ensure that you are ahead of the pack and are able to turn your challenges into solutions.</p>
</div>
<div class="testimonial-image"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/styles/testimonial/public/woman.png?itok=DDjuh7Wq" width="150" height="150" alt="" /></div>
<div class="testimonial-info">
Best Practices<br/>Understanding Trends
</div>
<div class="clearer"></div>
</div></span>  </div>    </div>
                  <div class="one_third views-column-2">

                    <div class="views-field views-field-title">        <span class="field-content"><div class="testimonial-item">
<div class="testimonial-body"><p>If you would like to be a part of the team and contribute to changing the landscape of IT in South Africa contact our Marketing Team.</p>
</div>
<div class="testimonial-image"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/styles/testimonial/public/telemarketer.png?itok=LV5FWdPv" width="150" height="150" alt="" /></div>
<div class="testimonial-info">
Grow our family<br/>Marketing Opportunities
</div>
<div class="clearer"></div>
</div></span>  </div>    </div>
                  <div class="one_third views-column-3 last">

                    <div class="views-field views-field-title">        <span class="field-content"><div class="testimonial-item">
<div class="testimonial-body"><p>We are passionate about building communities and generating interactive dialogue both technically and strategically with our #SmarterCommunities</p>
</div>
<div class="testimonial-image"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/styles/testimonial/public/man.png?itok=u1eEu5Tk" width="150" height="150" alt="" /></div>
<div class="testimonial-info">
Want to learn more?<br/>Community Opportunities
</div>
<div class="clearer"></div>
</div></span>  </div>    </div>
                </div>
              </div>
            </div>






          </div>  </div>
      </div>
    </div>
  </div>
</div>


<div id="footer-wrap" class="footer-area">
  <div id="footer" >
    <div class="container">
      <div class="footer-content">

        <div class="two_thirds">
          <div class="one_fourth">  <div class="region region-footer-links1">
            <div id="block-block-3" class="block block-block">


              <div class="content">
                <p> </p>
              </div>
            </div>
          </div>
          </div>
          <div class="one_fourth">  <div class="region region-footer-links2">
            <div id="block-views-footer-links-block" class="block block-views">


              <div class="content">
                <div class="view view-footer-links view-id-footer_links view-display-id-block footer-links view-dom-id-d7e61b5e95c9d983217883260084bcf4">
                  <div class="view-header">
                    <p><strong><a href="/products">Products</a></strong></p>
                  </div>



                  <div class="view-content">
                    <div class="views-row views-row-1 views-row-odd views-row-first">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/products/smarterteams">SmarterTeams</a></span>  </div>  </div>
                    <div class="views-row views-row-2 views-row-even">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/products/smartercode">SmarterCode</a></span>  </div>  </div>
                    <div class="views-row views-row-3 views-row-odd">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/products/smartercompute">SmarterCompute</a></span>  </div>  </div>
                    <div class="views-row views-row-4 views-row-even views-row-last">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/products/smarterdata">SmarterData</a></span>  </div>  </div>
                  </div>






                </div>  </div>
            </div>
          </div>
          </div>
          <div class="one_fourth">  <div class="region region-footer-links3">
            <div id="block-views-footer-links-block-2" class="block block-views">


              <div class="content">
                <div class="view view-footer-links view-id-footer_links view-display-id-block_2 footer-links view-dom-id-47741319fc5791f05c114e958b2b9e4d">
                  <div class="view-header">
                    <p><strong>Important Links</strong></p>
                  </div>



                  <div class="view-content">
                    <div class="views-row views-row-1 views-row-odd views-row-first">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/privacy-policy">Privacy Policy</a></span>  </div>  </div>
                    <div class="views-row views-row-2 views-row-even views-row-last">

                      <div class="views-field views-field-title">        <span class="field-content"><a href="/terms-and-conditions">Terms And Conditions</a></span>  </div>  </div>
                  </div>






                </div>  </div>
            </div>
          </div>
          </div>
          <div class="one_fourth last">  <div class="region region-footer-contact">
            <div id="block-views-footer-contact-details-block" class="block block-views">


              <div class="content">
                <div class="view view-footer-contact-details view-id-footer_contact_details view-display-id-block view-dom-id-5f20463e5553c3c6254255db243d92fb">



                  <div class="view-content">
                    <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">

                      <div class="views-field views-field-nothing">        <span class="field-content"><div class="view-header"><strong>Phone Numbers</strong></div></span>  </div>
                      <div class="views-field views-field-field-phone-number">        <div class="field-content"><a href="tel:08604LINUX%280860454689%29"> 0860 4 LINUX (0860 4 54689)</a><br /><a href="tel:%2B27117950200"> +27 11 795 0200</a></div>  </div>
                      <div class="views-field views-field-nothing-1">        <span class="field-content"><div class="view-header margin-top"><strong>E-mail Address</strong></div></span>  </div>
                      <div class="views-field views-field-field-e-mail-address">        <div class="field-content"><a href="mailto:info@obsidian.co.za">info@obsidian.co.za</a></div>  </div>  </div>
                  </div>






                </div>  </div>
            </div>
          </div>
          </div>
          <div class="clear"></div>
        </div>

        <div class="one_third last">
          <div class="region region-footer-social-copy">
            <div id="block-views-47eb673f4429efe7efd94b6e7c7a2d2b" class="block block-views">


              <div class="content">
                <div class="view view-footer-copyright-and-social view-id-footer_copyright_and_social view-display-id-block view-dom-id-f70b1cbf0396a618b10bb9b06f072f28">
                  <div class="view-header">
                    <div class="footer-image"><img typeof="foaf:Image" src="https://obsidian.co.za/sites/default/files/obs-logo-color.png" width="411" height="89" alt="" /></div>
                    <div class="footer-copyright">© 1995-2020 Obsidian Systems<br />All rights reserved</div>
                  </div>



                  <div class="view-content">
                    <div class="views-row views-row-1 views-row-odd views-row-first footer-social-item-wrap">

                      <div class="views-field views-field-field-sl-icon">        <div class="field-content"><a href="https://twitter.com/obsidianza" title="Twitter" target="_blank"><div class="footer-social-item"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></div>  </div>  </div>
                    <div class="views-row views-row-2 views-row-even footer-social-item-wrap">

                      <div class="views-field views-field-field-sl-icon">        <div class="field-content"><a href="https://www.facebook.com/obsidianza/" title="Facebook" target="_blank"><div class="footer-social-item"><i class="fa fa-facebook-f" aria-hidden="true"></i></div></a></div>  </div>  </div>
                    <div class="views-row views-row-3 views-row-odd footer-social-item-wrap">

                      <div class="views-field views-field-field-sl-icon">        <div class="field-content"><a href="https://www.instagram.com/obsidianza/" title="Instagram" target="_blank"><div class="footer-social-item"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></div>  </div>  </div>
                    <div class="views-row views-row-4 views-row-even views-row-last footer-social-item-wrap">

                      <div class="views-field views-field-field-sl-icon">        <div class="field-content"><a href="https://www.youtube.com/channel/UCUZyy2fkUq6SMK9bxeYzNlg" title="YouTube" target="_blank"><div class="footer-social-item"><i class="fa fa-youtube" aria-hidden="true"></i></div></a></div>  </div>  </div>
                  </div>






                </div>  </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>


        <div class="clear"></div>

      </div>
    </div>
  </div>
</div> <!-- /#wrap -->  </body>
</html>
